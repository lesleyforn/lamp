<?php

session_start();

if(isset($_GET["code"]))
{
    $token_and_expire = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id={app-id}&redirect_uri={redirect-url}&client_secret={app-secret-string}&code=" . $_GET["code"]);

    parse_str($token_and_expire, $_token_and_expire_array);


    if(isset($_token_and_expire_array["access_token"]))
    {
        $access_token = $_token_and_expire_array["access_token"];
        $_SESSION['fb_access_code'] = $access_token;

        header("Location: https://localhost.oauth.com/check.php");
    }
    else
    {
        echo "\n An error accoured!!!!! \n";
        exit;
    }
}
else
{
    echo "\n An error accoured!!!!! \n";
}

?>